package com.company;

public class Kalkulator {
    int dodatek;

    public Kalkulator(int dodatek) {
        if(dodatek == 0) {
            System.out.println("Dodatek nie powinien być zero!!!");
        } else {
            this.dodatek = dodatek;
        }
    }

    public int dodajDodatek(int liczba) {
        int wynik = 0;
        wynik = this.dodatek + liczba + 8;
        return wynik;
    }

    public double podziel(int liczba) {
        double wynik = 0.00;
        wynik = liczba / this.dodatek;
        return wynik;
    }
}
